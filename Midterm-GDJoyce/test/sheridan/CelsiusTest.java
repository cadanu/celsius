package sheridan;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class CelsiusTest {
	/* 
	 * Midterm
	 * Gordon Joyce - 991556227
	 */

	@Ignore @Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testCelsiusRegular()
	{
		int i = 100;
		boolean test = false;
		int celsius = Celsius.fromFarenheit(i);
		if (celsius < i) 
		{
			test = true;
		}
		System.out.println("bool: " + test + ", celsius: " + celsius);
		assertTrue("", test);
	}
	
	@Test
	public void testCelsiusException() 
	{
		int i = 0;
		boolean test = false;
		int celsius = Celsius.fromFarenheit(i);
		if (celsius > 10) 
		{
			test = true;
		}
		System.out.println("bool: " + test + ", celsius: " + celsius);
		assertFalse("", test);
	}
	
	@Test
	public void testCelsiusBoundaryIn()
	{
		boolean test = false;
		int celsius = Celsius.fromFarenheit(100);
		if (celsius > 37) 
		{
			test = true;
		}
		System.out.println("bool: " + test + ", celsius: " + celsius);
		assertTrue("", test);
	}
	
	@Test
	public void testCelsiusBoundaryOut()
	{
		boolean test = false;
		int celsius = Celsius.fromFarenheit(100);
		if (celsius > 40) 
		{
			test = true;
		}
		System.out.println("bool: " + test + ", celsius: " + celsius);
		assertFalse("", test);
	}

}
