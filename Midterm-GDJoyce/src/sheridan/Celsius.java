package sheridan;

public class Celsius {
	/* 
	 * Midterm
	 * Gordon Joyce - 991556227
	 */
	
	public static int fromFarenheit(int faren)
	{
		double celsius = ((double)faren - 32.0) * (5.0/9.0);
		System.out.println("celsius: " + celsius + ", faren: " + faren);
		int rounded = (int)(Math.ceil(celsius));
		return rounded;
	}
}
